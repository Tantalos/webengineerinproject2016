package de.unipassau.webproject;

import java.sql.SQLException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import de.unipassau.webproject.dataaccess.ConnectionPool;

public class JSFPollContextListener implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        try {
            ConnectionPool.getInstance().close(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        ConnectionPool.getInstance();
    }

}
