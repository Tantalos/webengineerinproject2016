package de.unipassau.webproject.utils;

import org.apache.commons.codec.digest.DigestUtils;

public class EncryptionUtils {



    public static String hashString(String seed) {
        return DigestUtils.sha256Hex(seed);
    }
}
