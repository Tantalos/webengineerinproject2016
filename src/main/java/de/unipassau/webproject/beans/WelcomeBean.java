package de.unipassau.webproject.beans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import de.unipassau.webproject.dto.SurveyDTO;
import de.unipassau.webproject.service.SurveyService;

@ManagedBean
@RequestScoped
public class WelcomeBean {
	public List<SurveyDTO> surveyDTOs;
	
	@PostConstruct
	public void getAllSurveyDTOs(){
		SurveyService surveyService = new SurveyService();
		this.surveyDTOs = surveyService.getAllSurveyDTOsFromAllUsers();
	}

	public List<SurveyDTO> getSurveyDTOs() {
		return surveyDTOs;
	}

	public void setSurveyDTOs(List<SurveyDTO> surveyDTOs) {
		this.surveyDTOs = surveyDTOs;
	}
}
