package de.unipassau.webproject.beans;

import java.util.LinkedList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import de.unipassau.webproject.dto.SurveyDTO;
import de.unipassau.webproject.dto.UserDTO;
import de.unipassau.webproject.dto.VoteDTO;
import de.unipassau.webproject.service.SurveyService;
import de.unipassau.webproject.service.VoteService;

@ManagedBean
@RequestScoped
public class VoteBean {

    private SurveyDTO survey;
    private long surveyID;
    private long creator;
    private List<VoteDTO> allVotes;
    private long voteCount = -1;
    private String voterName = null;


    private Long questionId1;
    private Long questionId2;
    private Long questionId3;

    private String questionText1;
    private String questionText2;
    private String questionText3;

    private boolean answerText1;
    private boolean answerText2;
    private boolean answerText3;

    public void init() {
    	SurveyService surveyService = new SurveyService();
    	this.survey = surveyService.getSurveyByID(surveyID);
    	this.questionId1 = survey.getQuestions().get(0).getQuestionId();
    	this.questionId2 = survey.getQuestions().get(1).getQuestionId();
    	this.questionId3 = survey.getQuestions().get(2).getQuestionId();
    	this.questionText1 = survey.getQuestions().get(0).getQuestionText();
    	this.questionText2 = survey.getQuestions().get(1).getQuestionText();
    	this.questionText3 = survey.getQuestions().get(2).getQuestionText();
    	HttpSession session = SessionBean.getSession();
    	UserDTO userDTO = (UserDTO)session.getAttribute("user");
        this.voterName = userDTO == null ? null : userDTO.getName();
    }

    public String outcome(long surveyID){
    	return "voteSurvey.xhtml?faces-redirect=true&includeViewParams=true&surveyID=" + surveyID;
    }

    public SurveyDTO getSurvey() {
        return survey;
    }

    public void setSurvey(SurveyDTO survey) {
        this.survey = survey;
    }

    public boolean isUser(){
    	HttpSession session = SessionBean.getSession();
    	UserDTO userDTO = (UserDTO)session.getAttribute("user");
        String voterNameForTest = userDTO == null ? null : userDTO.getName();
        if(voterNameForTest == null){
        	return false;
        } else {
        	this.voterName = voterNameForTest;
        	return true;
        }
    }

    public String createVote(){
    	if(this.voterName == null || this.voterName.isEmpty()){
    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "Pls insert name", ""));
    		return "voteSurvey?faces-redirect=true&includeViewParams=true&surveyID=" + surveyID;
    	}

    	VoteService voteService = new VoteService();
    	List<Boolean> answerList = new LinkedList<>();
    	//list for iterate the values for votesDTO
    	answerList.add(this.answerText1);
    	answerList.add(this.answerText2);
    	answerList.add(this.answerText3);

    	SurveyService surveyService = new SurveyService();
    	this.survey = surveyService.getSurveyByID(this.surveyID);
        boolean success = voteService.createVote(this.voterName, this.survey, answerList);
    	if(!isUser()){
            return success ? "login" : "voteSurvey?faces-redirect=true&includeViewParams=true&surveyID=" + surveyID;
        } else {
            return success ? "survey" : "voteSurvey?faces-redirect=true&includeViewParams=true&surveyID=" + surveyID;
        }
    }

    public long getCreator() {
        return creator;
    }

    public void setCreator(long creator) {
        this.creator = creator;
    }

    public List<VoteDTO> getAllVotes() {
        return allVotes;
    }

    public void setAllVotes(List<VoteDTO> allVotes) {
        this.allVotes = allVotes;
    }

    public long getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(long voteCount) {
        this.voteCount = voteCount;
    }

	public String getVoterName() {
		return voterName;
	}

	public void setVoterName(String voterName) {
		this.voterName = voterName;
	}

	public Long getQuestionId1() {
		return questionId1;
	}

	public Long getQuestionId2() {
		return questionId2;
	}

	public Long getQuestionId3() {
		return questionId3;
	}

	public String getQuestionText1() {
		return questionText1;
	}

	public String getQuestionText2() {
		return questionText2;
	}

	public String getQuestionText3() {
		return questionText3;
	}

	public void setQuestionId1(Long questionId1) {
		this.questionId1 = questionId1;
	}

	public void setQuestionId2(Long questionId2) {
		this.questionId2 = questionId2;
	}

	public void setQuestionId3(Long questionId3) {
		this.questionId3 = questionId3;
	}

	public void setQuestionText1(String questionText1) {
		this.questionText1 = questionText1;
	}

	public void setQuestionText2(String questionText2) {
		this.questionText2 = questionText2;
	}

	public void setQuestionText3(String questionText3) {
		this.questionText3 = questionText3;
	}

	public long getSurveyID() {
		return surveyID;
	}

	public void setSurveyID(long surveyID) {
		this.surveyID = surveyID;
	}

	public boolean isAnswerText1() {
		return answerText1;
	}

	public boolean isAnswerText2() {
		return answerText2;
	}

	public boolean isAnswerText3() {
		return answerText3;
	}

	public void setAnswerText1(boolean answerText1) {
		this.answerText1 = answerText1;
	}

	public void setAnswerText2(boolean answerText2) {
		this.answerText2 = answerText2;
	}

	public void setAnswerText3(boolean answerText3) {
		this.answerText3 = answerText3;
	}


}
