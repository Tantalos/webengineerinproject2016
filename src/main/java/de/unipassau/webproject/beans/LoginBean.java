package de.unipassau.webproject.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.HttpSession;

import de.unipassau.webproject.dto.UserDTO;
import de.unipassau.webproject.service.LoginService;

@ManagedBean
@SessionScoped
public class LoginBean {

    private String pwd;
    private String user;
    private UserDTO userDTO;
    public boolean logedIn;

    public boolean isLogedIn() {
		return logedIn;
	}

	public void setLogedIn(boolean logedIn) {
		this.logedIn = logedIn;
	}

	public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }


    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    //validate login
    public String checkLogin() {
    	UserDTO userDTO = new UserDTO(user, pwd, "");
        LoginService loginService = new LoginService();

        if(loginService.checkPassword(userDTO)){
        	this.logedIn = true;
        	return "survey.xthml";
        } else {
        	return "login.xhtml";
        }
    }

    //logout event, invalidate session
    public String logout() {
        HttpSession session = SessionBean.getSession();
        session.invalidate();
        this.logedIn = false;
        return "login";
    }
}