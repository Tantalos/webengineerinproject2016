package de.unipassau.webproject.beans;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.servlet.http.HttpSession;

import de.unipassau.webproject.dto.QuestionDTO;
import de.unipassau.webproject.dto.SurveyDTO;
import de.unipassau.webproject.dto.UserDTO;
import de.unipassau.webproject.service.SurveyService;

@ManagedBean
@RequestScoped
public class SurveyCreationBean {
    private String title;
    private Long surveyId = null;
    private Long questionId1 = null;
    private Long questionId2 = null;
    private Long questionId3 = null;
    private Timestamp creationDate = null;

    private String questionText1;
    private String questionText2;
    private String questionText3;
    
    public String editOwnSurvey() {
        SurveyService surveyService = new SurveyService();
        SurveyDTO newSurvey = createSurveyDTO();
        newSurvey.setCreationDate(creationDate);

        HttpSession session = SessionBean.getSession();
        return surveyService.edit(newSurvey, (UserDTO) session.getAttribute("user"));
    }

    public void setSurveyIdFromOwnSurveyList(long surveyId) {
    	this.surveyId = surveyId;
    }

    public String create() {
        SurveyService surveyService = new SurveyService();
        SurveyDTO newSurvey = createSurveyDTO();

        return surveyService.addSurvey(newSurvey);
    }

    private static java.sql.Timestamp getCurrentTimeStamp() {

    	java.util.Date today = new java.util.Date();
    	return new java.sql.Timestamp(today.getTime());

    }


    private SurveyDTO createSurveyDTO() {
        HttpSession session = SessionBean.getSession();
        UserDTO user = (UserDTO) session.getAttribute("user");
        List<QuestionDTO> questions = new ArrayList<>();
        questions.add(new QuestionDTO(questionId1, surveyId, questionText1));
        questions.add(new QuestionDTO(questionId2, surveyId, questionText2));
        questions.add(new QuestionDTO(questionId3, surveyId, questionText3));

        return new SurveyDTO(surveyId, user.getUserId(), null, getCurrentTimeStamp(), questions, title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setQuestionText1(String questionText1) {
        this.questionText1 = questionText1;
    }

    public void setQuestionText2(String questionText2) {
        this.questionText2 = questionText2;
    }

    public void setQuestionText3(String questionText3) {
        this.questionText3 = questionText3;
    }

    public Long getSurveyId() {
		return surveyId;
	}

    public void setSurveyId(Long surveyId) {
        this.surveyId = surveyId;
    }

    public String getTitle() {
		return title;
	}
	public String getQuestionText1() {
		return questionText1;
	}
	public String getQuestionText2() {
		return questionText2;
	}
	public String getQuestionText3() {
		return questionText3;
	}

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Long getQuestionId1() {
        return questionId1;
    }

    public void setQuestionId1(Long questionId1) {
        this.questionId1 = questionId1;
    }

    public Long getQuestionId2() {
        return questionId2;
    }

    public void setQuestionId2(Long questionId2) {
        this.questionId2 = questionId2;
    }

    public Long getQuestionId3() {
        return questionId3;
    }

    public void setQuestionId3(Long questionId3) {
        this.questionId3 = questionId3;
    }
}
