package de.unipassau.webproject.beans;

import java.sql.Timestamp;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean
@ApplicationScoped
public class UtilsBean {
    public String convertTime(Timestamp time) {

        Date date = new Date(time.getTime());
        Format format = new SimpleDateFormat("dd.MM.yyyy   HH:mm:ss");
        return format.format(date);
    }
}
