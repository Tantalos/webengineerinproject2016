package de.unipassau.webproject.beans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import de.unipassau.webproject.dto.SurveyDTO;
import de.unipassau.webproject.dto.UserDTO;
import de.unipassau.webproject.service.SurveyService;

@ManagedBean
@RequestScoped
public class SurveyBean {

	private List<SurveyDTO> ownSurveys;
	private List<SurveyDTO> allSurveys;


	@PostConstruct
	public void fetchOwnSurveys(){
		SurveyService surveyService = new SurveyService();
		HttpSession session = SessionBean.getSession();
		this.ownSurveys = surveyService.getOwnSurveyDTOs((UserDTO) session.getAttribute("user"));
		this.allSurveys = surveyService.getAllSurveyDTOs((UserDTO) session.getAttribute("user"));
	}

    public String isEditable(SurveyDTO surveyDTO) {
        SurveyService surveyService = new SurveyService();
        if (!surveyService.isSurveyChangeable(surveyDTO)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "There are already votes for this surey. You can't edit the survey anymore!", ""));
            return "";
        } else {
        	return "editSurvey";
        }
    }

    public String removeOwnSurvey(SurveyDTO survey) {
        SurveyService surveyService = new SurveyService();
        HttpSession session = SessionBean.getSession();
        return surveyService.delete(survey, (UserDTO) session.getAttribute("user"));
    }

    public String isVotetable(SurveyDTO surveyDTO){
    	SurveyService surveyService = new SurveyService();
    	HttpSession session = SessionBean.getSession();
    	if(surveyService.isSurveyVoteableFromUser(surveyDTO, ((UserDTO) session.getAttribute("user")).getName())){
    		return "voteSurvey.xhtml?faces-redirect=true&includeViewParams=true&surveyID=" + surveyDTO.getSurveyId();
    	} else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "You voted already for this survey!", ""));
    		return "";
    	}
    }


	public List<SurveyDTO> getSurvey() {
		return ownSurveys;
	}

	public void setSurvey(List<SurveyDTO> survey) {
		this.ownSurveys = survey;
	}
	public void setOwnSurveys(List<SurveyDTO> ownSurveys) {
		this.ownSurveys = ownSurveys;
	}

	public void setAllSurveys(List<SurveyDTO> allSurveys) {
		this.allSurveys = allSurveys;
	}
	public List<SurveyDTO> getOwnSurveys() {
		return ownSurveys;
	}
	public List<SurveyDTO> getAllSurveys() {
		return allSurveys;
	}

}
