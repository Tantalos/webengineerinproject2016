package de.unipassau.webproject.beans;

import java.util.List;

import de.unipassau.webproject.dto.QuestionResultDTO;
import de.unipassau.webproject.dto.SurveyDTO;
import de.unipassau.webproject.service.VoteService;

public class VoteStatisticBean {
	
	private SurveyDTO surveyDTO;
	private List<QuestionResultDTO> questionResultDTOs;
	
	public void count(){
		VoteService voteService = new VoteService();
		this.questionResultDTOs = voteService.getStatistic(surveyDTO);
	}
	
	public String backButton(){
		return "survey";
	}

	public SurveyDTO getSurveyDTO() {
		return surveyDTO;
	}

	public void setSurveyDTO(SurveyDTO surveyDTO) {
		this.surveyDTO = surveyDTO;
	}

	public List<QuestionResultDTO> getQuestionResultDTOs() {
		return questionResultDTOs;
	}
	
	
}
