package de.unipassau.webproject.dataaccess;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.MissingResourceException;
import java.util.Properties;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;

public class ConnectionPool {
    private static ConnectionPool INSTANCE;

    private final String DRIVER_CLASS;
    private final String JDBC_URL;
    private final String USER;
    private final String PASSWORD;

    private final int MIN_POOL_SIZE;
    private final int MAX_POOL_SIZE;

    private final LinkedList<Connection> connectionsAvailable = new LinkedList<Connection>();
    private final LinkedList<Connection> connectionsUsed = new LinkedList<Connection>();

    private PoolBoy connectionRelease;

    /*
     * This variable is necessary if connections are managed concurrently. It is
     * not possible to use the connectionAvailable size and connectionUsed size
     * because concurrent modification may corrupt the information
     */
    private volatile int connectionCount = 0;

    /**
     * Constructor of the Singleton ConnectionPool.
     */
    private ConnectionPool() {
        Properties databaseCredentials = loadDatabaseCredentials();

        DRIVER_CLASS = databaseCredentials.getProperty("database.driverClass");
        JDBC_URL = databaseCredentials.getProperty("database.jdbcUrl");
        USER = databaseCredentials.getProperty("database.user");
        PASSWORD = databaseCredentials.getProperty("database.password");

        // set the pool size
        MIN_POOL_SIZE = Integer.valueOf(databaseCredentials.getProperty("database.minPoolSize"));
        MAX_POOL_SIZE = Integer.valueOf(databaseCredentials.getProperty("database.maxPoolSize"));

    }

    private Properties loadDatabaseCredentials() {
        Properties databaseCredentials = new Properties();
        InputStream in = getClass().getClassLoader().getResourceAsStream("database.properties");
        try {
            databaseCredentials.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return databaseCredentials;
    }

    private Connection createNewConnection() throws SQLException {

        Connection connection = DriverManager.getConnection(JDBC_URL, USER, PASSWORD);
        connectionCount++;
        connection.setAutoCommit(false);
        connectionsAvailable.add(connection);
        return connection;
    }

    public static ConnectionPool getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ConnectionPool();
            try {
                INSTANCE.initialize();
            } catch (SQLException e) {
                throw new RuntimeException("Conneciton pool could not be initialized. Check your database credentials",
                        e);
            }
        }
        return INSTANCE;
    }

    public void initialize() throws SQLException {
        try {
            Class.forName(DRIVER_CLASS);
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        }

        for (int i = 0; i < MIN_POOL_SIZE; i++) {
            createNewConnection();
        }
        connectionRelease = new PoolBoy();
        connectionRelease.start();
    }

    private Connection useConnection(Connection connection) {
        Preconditions.checkNotNull(connection);
        assert connectionsAvailable.contains(connection) : "requested connection is not in available set. "
                + "Connection pool state is corrupt!";
        assert !connectionsUsed.contains(connection) : "requested connection is busy. "
                + "Connection pool state is corrupt";

        connectionsAvailable.remove(connection);
        connectionsUsed.add(connection);

        return connection;
    }

    public void releaseConnection(Connection connection) {
        Preconditions.checkNotNull(connection);
        assert !connectionsAvailable.contains(connection) : "released connection was noted as available. "
                + "Connection pool state is corrupt";
        assert connectionsUsed.contains(connection) : "released connection was not in use. "
                + "Connection pool state is corrupt";

        connectionsUsed.remove(connection);
        connectionsAvailable.add(connection);
    }

    public void close(boolean force) throws SQLException {
        if (!force) {
            if (!connectionsUsed.isEmpty()) {
                throw new ConcurrentModificationException(
                        "Connection was in used while trying to close connection pool");
            }
        }
        for (Connection connection : connectionsAvailable) {
            connection.close();
        }
        for (Connection connection : connectionsUsed) {
            connection.close();
        }
        connectionRelease.kill();
    }

    @Nonnull
    public Connection getConnection() throws MissingResourceException, SQLException {
        if (!connectionsAvailable.isEmpty()) {
            return useConnection(connectionsAvailable.peek());
        } else {
            assert connectionsAvailable.size() == 0;
            assert connectionsUsed.size() == connectionCount;

            if (connectionCount >= MAX_POOL_SIZE) {
                throw new MissingResourceException(
                        "No connections left. Increase the max database connection or wait util connections are released!",
                        Connection.class.toString(), "");
            } else {
                return useConnection(createNewConnection());
            }
        }
    }

    public int getAvailableConnectionSize() {
        return connectionsAvailable.size();
    }

    public int getUsedConnectionSize() {
        return connectionsUsed.size();
    }

    /**
     * This watch dog will release connection resources asynchronously. The
     * release is then not done during connection fetch
     */
    public class PoolBoy extends Thread {
        private boolean isAlive = true;

        @Override
        public void run() {
            while (isAlive) {
                try {
                    cleanPool();
                    Thread.sleep(2 * 1000);
                } catch (InterruptedException | SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * will release unused connections. Will release connections slowly, not
         * all at once!!
         */
        public void cleanPool() throws SQLException {
            synchronized (connectionsAvailable) {
                if (connectionsAvailable.size() > MIN_POOL_SIZE) {
                    connectionsAvailable.poll().close();
                    connectionCount--;
                }
            }
        }

        public void kill() {
            isAlive = false;
        }
    }

}