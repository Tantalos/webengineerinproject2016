package de.unipassau.webproject.dataaccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import de.unipassau.webproject.dto.QuestionResultDTO;
import de.unipassau.webproject.dto.VoteDTO;

public class VoteDAO {

    public List<VoteDTO> getVoteOfSurvey(long surveyId, Connection connection) throws SQLException {
        String sqlQuery = "SELECT vote_id, voter, answer, creation_date, question FROM vote INNER JOIN (SELECT * FROM question WHERE survey = ?) as a ON a.question_id = vote.question;";
        PreparedStatement queryTemplate = connection.prepareStatement(sqlQuery);
        List<VoteDTO> voteOfSurvey = new ArrayList<VoteDTO>();
        queryTemplate.setLong(1, surveyId);

        ResultSet result = queryTemplate.executeQuery();

        while (result.next()) {
            long voteId = result.getLong("VOTE_ID");
            String voter = result.getString("VOTER");
            long survey = result.getLong("QUESTION");
            Timestamp creationDate = result.getTimestamp("CREATION_DATE");
            boolean answer = result.getBoolean("answer");

            voteOfSurvey.add(new VoteDTO(voteId, voter, survey, answer, creationDate));
        }
        return voteOfSurvey;
    }

    public void addVote(List<VoteDTO> votes, Connection connection) throws SQLException {
    	for (VoteDTO vote : votes) {
    		String sqlQuery = "INSERT INTO vote (voter, question, answer, creation_date) VALUES(?,?,?,?);\n";
            PreparedStatement queryTemplate;
            queryTemplate = connection.prepareStatement(sqlQuery);
            queryTemplate.setString(1, vote.getVoter());
            queryTemplate.setLong(2, vote.getQuestion());
            queryTemplate.setBoolean(3, vote.isAnswer());
            queryTemplate.setTimestamp(4, vote.getTimestamp());

            queryTemplate.executeUpdate();
		}



    }

    public QuestionResultDTO getQuestionResult(long questionId, Connection connection) throws SQLException {
        String sqlQuery = "SELECT answer, count(answer) FROM vote WHERE question = ? GROUP BY answer;";
        PreparedStatement queryTemplate = connection.prepareStatement(sqlQuery);
        queryTemplate.setLong(1, questionId);

        ResultSet result = queryTemplate.executeQuery();

        int resultTrue = 0;
        int resultFalse = 0;

        while (result.next()) {
            boolean answer = result.getBoolean("answer");
            if (answer) {
                resultTrue = result.getInt("count");
            } else {
                assert !answer;
                resultFalse = result.getInt("count");
            }
        }


        return new QuestionResultDTO(questionId, resultTrue, resultFalse);
    }

    public int getVoteAmountOfSurvey(long surveyId, Connection connection) throws SQLException {
        String sqlQuery = "SELECT count(vote_id) FROM vote JOIN (SELECT * FROM question WHERE survey = ?) as question_of_survey ON question_of_survey.question_id = vote.question;";
        PreparedStatement queryTemplate = connection.prepareStatement(sqlQuery);
        queryTemplate.setLong(1, surveyId);

        ResultSet result = queryTemplate.executeQuery();

        if (!result.next()) {
            return -1;
        }

        return result.getInt("count");
    }
}
