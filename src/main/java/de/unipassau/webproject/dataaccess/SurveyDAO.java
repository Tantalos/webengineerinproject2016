package de.unipassau.webproject.dataaccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import de.unipassau.webproject.dto.QuestionDTO;
import de.unipassau.webproject.dto.SurveyDTO;
import de.unipassau.webproject.dto.UserDTO;

public class SurveyDAO {
    private QuestionDAO questionDao = new QuestionDAO();


    public List<SurveyDTO> getSurveyByUserName(UserDTO user, Connection connection) throws SQLException {
        List<SurveyDTO> surveyDTOs = new ArrayList<>();

        String sqlQuery = "SELECT * FROM survey where CREATOR = ?;";
        PreparedStatement queryTemplate = connection.prepareStatement(sqlQuery);
        queryTemplate.setLong(1, user.getUserId());

        ResultSet result = queryTemplate.executeQuery();

        while (result.next()) {
            long surveyId = result.getLong("SURVEY_ID");
            long creatorId = result.getLong("CREATOR");
            Timestamp creationDate = result.getTimestamp("CREATION_DATE");
            String title = result.getString("TITLE");
            List<QuestionDTO> questions = questionDao.getQuestionBySurveyID(surveyId, connection);

            SurveyDTO surveyDTO = new SurveyDTO(surveyId, creatorId, user.getName(), creationDate, questions, title);
            surveyDTOs.add(surveyDTO);
        }
        return surveyDTOs;
    }

    public void addSurvey(SurveyDTO survey, Connection connection) throws SQLException {
        String sqlQuery = "INSERT INTO survey (creator, creation_date, title) VALUES (?, ?, ?);";
        PreparedStatement queryTemplate = connection.prepareStatement(sqlQuery, PreparedStatement.RETURN_GENERATED_KEYS);
        queryTemplate.setLong(1, survey.getCreator());
        queryTemplate.setTimestamp(2, survey.getCreationDate());
        queryTemplate.setString(3, survey.getTitle());
        int affectedRows = queryTemplate.executeUpdate();

        if (affectedRows == 0) {
            throw new SQLException("Creating survey failed, no rows affected.");
        }
        ResultSet generatedKeys = queryTemplate.getGeneratedKeys();
        long surveyKey = -1;
        connection.commit();

        if (generatedKeys.next()) {
            surveyKey = generatedKeys.getLong(1);
        } else {
            throw new SQLException("Error inserting survey. There was no generated Key");
        }

        for (QuestionDTO question : survey.getQuestions()) {
            question.setSurveyId(surveyKey);
            questionDao.addNewQuestion(question, connection);
        }
    }
   public void updateSurvey(SurveyDTO survey, Connection connection) throws SQLException {

        for (QuestionDTO question : survey.getQuestions()) {
            questionDao.updateQuestion(question, connection);
        }

        String sqlQuery = "UPDATE survey SET creator = ?, creation_date = ?, title = ? WHERE survey_id = ?;";

        PreparedStatement queryTemplate = connection.prepareStatement(sqlQuery);
        queryTemplate.setLong(1, survey.getCreator());
        queryTemplate.setTimestamp(2, survey.getCreationDate());
        queryTemplate.setString(3, survey.getTitle());

        queryTemplate.setLong(4, survey.getSurveyId());

        queryTemplate.executeUpdate();
    }

   public List<SurveyDTO> getALLSurveys(UserDTO user, Connection connection) throws SQLException {
       	QuestionDAO questionDao = new QuestionDAO();
        UserDAO userDAO = new UserDAO();
        List<SurveyDTO> surveyDTOs = new ArrayList<>();

        String sqlQuery = "SELECT * FROM survey where creator <> (?);";
        PreparedStatement queryTemplate = connection.prepareStatement(sqlQuery);
        queryTemplate.setLong(1, user.getUserId());
        ResultSet result = queryTemplate.executeQuery();

        while (result.next()) {
            long surveyId = result.getLong("SURVEY_ID");
            long creatorId = result.getLong("CREATOR");
            Timestamp creationDate = result.getTimestamp("CREATION_DATE");
            String title = result.getString("TITLE");
            String creatorName = userDAO.getUserById(creatorId, connection).getName();
            List<QuestionDTO> questions = questionDao.getQuestionBySurveyID(surveyId, connection);

            SurveyDTO surveyDTO = new SurveyDTO(surveyId, creatorId, creatorName, creationDate, questions, title);
            surveyDTOs.add(surveyDTO);
        }
        return surveyDTOs;
    }

   public List<SurveyDTO> getALLSurveysFromAll(Connection connection) throws SQLException {
      	QuestionDAO questionDao = new QuestionDAO();
        UserDAO userDAO = new UserDAO();
       List<SurveyDTO> surveyDTOs = new ArrayList<>();

       String sqlQuery = "SELECT * FROM survey;";
       PreparedStatement queryTemplate = connection.prepareStatement(sqlQuery);
       ResultSet result = queryTemplate.executeQuery();

       while (result.next()) {
           long surveyId = result.getLong("SURVEY_ID");
           long creatorId = result.getLong("CREATOR");
           Timestamp creationDate = result.getTimestamp("CREATION_DATE");
           String title = result.getString("TITLE");
            String creatorName = userDAO.getUserById(creatorId, connection).getName();
           List<QuestionDTO> questions = questionDao.getQuestionBySurveyID(surveyId, connection);

            SurveyDTO surveyDTO = new SurveyDTO(surveyId, creatorId, creatorName, creationDate, questions, title);
           surveyDTOs.add(surveyDTO);
       }
       return surveyDTOs;
   }

    public void deleteSurvey(SurveyDTO surveyDTO, Connection connection) throws SQLException {
        for (QuestionDTO question : surveyDTO.getQuestions()) {
            questionDao.deleteQuestion(question, connection);
        }
        String sqlQuery = "DELETE FROM survey WHERE survey_id = ?;";
        PreparedStatement queryTemplate = connection.prepareStatement(sqlQuery,
                PreparedStatement.RETURN_GENERATED_KEYS);
        queryTemplate.setLong(1, surveyDTO.getSurveyId());

        queryTemplate.executeUpdate();
    }

    @Deprecated
    public List<SurveyDTO> getALLOtherSurveys(UserDTO user, Connection connection) {
        return null;
    }

}
