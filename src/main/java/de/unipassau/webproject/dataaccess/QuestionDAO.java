package de.unipassau.webproject.dataaccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import de.unipassau.webproject.dto.QuestionDTO;

public class QuestionDAO {

    @Nonnull
    public List<QuestionDTO> getQuestionBySurveyID(long surveyId, Connection connection) throws SQLException {
        String sqlQuery = "SELECT * FROM question where survey = ?;";
        List<QuestionDTO> questionDTOList = new ArrayList<>();
        PreparedStatement queryTemplate = connection.prepareStatement(sqlQuery);
        queryTemplate.setLong(1, surveyId);

        ResultSet resultSet = queryTemplate.executeQuery();

        while (resultSet.next()) {
            long questionId = resultSet.getLong("QUESTION_ID");
            long surveyIdRef = resultSet.getLong("SURVEY");
            String questionText = resultSet.getString("QUESTION_TEXT");
            QuestionDTO questionDTO = new QuestionDTO(questionId, surveyIdRef, questionText);
            questionDTOList.add(questionDTO);
        }

        return questionDTOList;
    }

    public void addNewQuestion(QuestionDTO question, Connection connection) throws SQLException {
        String sqlQuery = "INSERT INTO question (survey, question_text) VALUES (?, ?);";
        PreparedStatement queryTemplate = connection.prepareStatement(sqlQuery);
        queryTemplate.setLong(1, question.getSurveyId());
        queryTemplate.setString(2, question.getQuestionText());

        queryTemplate.executeUpdate();
    }

    public void updateQuestion(QuestionDTO question, Connection connection) throws SQLException {
        String sqlQuery = "UPDATE question SET survey = ?, question_text = ? WHERE question_id = ?;";
        PreparedStatement queryTemplate = connection.prepareStatement(sqlQuery);
        queryTemplate.setLong(1, question.getSurveyId());
        queryTemplate.setString(2, question.getQuestionText());
        queryTemplate.setLong(3, question.getQuestionId());

        queryTemplate.executeUpdate();
    }

    public void deleteQuestion(QuestionDTO question, Connection connection) throws SQLException {
        String sqlQuery = "DELETE FROM question WHERE survey = ?;";
        PreparedStatement queryTemplate = connection.prepareStatement(sqlQuery);
        queryTemplate.setLong(1, question.getSurveyId());

        queryTemplate.executeUpdate();
    }

}
