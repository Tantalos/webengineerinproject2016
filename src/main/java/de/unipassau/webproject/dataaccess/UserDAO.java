package de.unipassau.webproject.dataaccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import de.unipassau.webproject.dto.UserDTO;

public class UserDAO {

    public UserDTO getUserByName(String username, Connection connection) throws SQLException {

        String sqlQuery = "SELECT * FROM online_user where USER_NAME = ?;";
        PreparedStatement queryTemplate = connection.prepareStatement(sqlQuery);
        queryTemplate.setString(1, username);

        // run sql query
        ResultSet result = queryTemplate.executeQuery();

        if (!result.next()) {
            return null;
        }

        long userid = result.getLong("user_id");
        String name = result.getString("USER_NAME");
        Timestamp birth = result.getTimestamp("BIRTH");
        String address = result.getString("ADDRESS");
        String passwordHash = result.getString("PASSWORDHASH");
        String salt = result.getString("SALT");
        return new UserDTO(userid, name, birth, address, passwordHash, salt);

    }

    public UserDTO getUserById(long userId, Connection connection) throws SQLException {
        String sqlQuery = "SELECT * FROM online_user where USER_ID = ?;";
        PreparedStatement queryTemplate = connection.prepareStatement(sqlQuery);
        queryTemplate.setLong(1, userId);

        // run sql query
        ResultSet result = queryTemplate.executeQuery();

        if (!result.next()) {
            return null;
        }

        long userid = result.getLong("user_id");
        String name = result.getString("USER_NAME");
        Timestamp birth = result.getTimestamp("BIRTH");
        String address = result.getString("ADDRESS");
        String passwordHash = result.getString("PASSWORDHASH");
        String salt = result.getString("SALT");
        return new UserDTO(userid, name, birth, address, passwordHash, salt);

    }

    public void createUser(UserDTO user, Connection connection) throws SQLException {
        String sqlQuery = "INSERT INTO online_user (user_name, birth, address, passwordhash, salt) VALUES(?,?,?,?,?);";
        PreparedStatement queryTemplate;
        queryTemplate = connection.prepareStatement(sqlQuery);
        queryTemplate.setString(1, user.getName());
        queryTemplate.setTimestamp(2, user.getBirth());
        queryTemplate.setString(3, user.getAddress());
        queryTemplate.setString(4, user.getPasswordHash());
        queryTemplate.setString(5, user.getSalt());

        queryTemplate.executeUpdate();
    }

}
