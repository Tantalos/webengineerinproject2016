package de.unipassau.webproject.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
public class QuestionResultDTO {
    @NonNull private Long questionId;
    private String questionText;
    @NonNull private Integer resultYes;
    @NonNull private Integer resultNo;

}
