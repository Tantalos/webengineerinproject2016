package de.unipassau.webproject.dto;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(of = {"name", "birth", "address"})
@AllArgsConstructor
@RequiredArgsConstructor
@ToString
public class UserDTO {
	
    private Long userId;
	@NonNull private String name;
	private Timestamp birth;
	private String address;
    @NonNull private String passwordHash;
    @NonNull private String salt;

}
