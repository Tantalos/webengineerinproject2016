package de.unipassau.webproject.dto;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@ToString
public class VoteDTO {
    private Long voteID;
    private String voter;
    private long question;
    private boolean answer;
    private Timestamp timestamp;
    
}
