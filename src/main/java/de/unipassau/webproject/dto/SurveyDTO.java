package de.unipassau.webproject.dto;

import java.sql.Timestamp;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
public class SurveyDTO {
    private Long surveyId;
    @NonNull private Long creator;
    private String creatorName;
    @NonNull private Timestamp creationDate;
    @NonNull private List<QuestionDTO> questions;
    @NonNull private String title;
}
