package de.unipassau.webproject.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import de.unipassau.webproject.dataaccess.ConnectionPool;
import de.unipassau.webproject.dataaccess.VoteDAO;
import de.unipassau.webproject.dto.QuestionDTO;
import de.unipassau.webproject.dto.QuestionResultDTO;
import de.unipassau.webproject.dto.SurveyDTO;
import de.unipassau.webproject.dto.VoteDTO;



public class VoteService {
    private ConnectionPool connectionPool = ConnectionPool.getInstance();
    private VoteDAO voteDAO = new VoteDAO();

    public long getVoteCount(SurveyDTO survey) {
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            int voteCount = voteDAO.getVoteAmountOfSurvey(survey.getSurveyId(), connection);
            connection.commit();
            connectionPool.releaseConnection(connection);
            return voteCount;
        } catch (SQLException e) {
            if (connection != null) {
                try {
                    connection.rollback();
                    connectionPool.releaseConnection(connection);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            e.printStackTrace();
            return -1;
        }
    }

    private static java.sql.Timestamp getCurrentTimeStamp() {

    	java.util.Date today = new java.util.Date();
    	return new java.sql.Timestamp(today.getTime());

    }

    public boolean createVote(String username, SurveyDTO surveyDTO, List<Boolean> answerList){
        Connection connection = null;
        SurveyService surveyService = new SurveyService();

        if (!surveyService.isSurveyVoteableFromUser(surveyDTO, username)) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "You voted already for this survey!", ""));
            return false;
        }

        try {
            connection = connectionPool.getConnection();
            List<VoteDTO>voteDTOs = new LinkedList<>();
            int iterateId = 0;
            for (Boolean answer : answerList) {
            	VoteDTO voteDTO = new VoteDTO(null, username, surveyDTO.getQuestions().get(iterateId).getQuestionId(), answer, getCurrentTimeStamp());
				voteDTOs.add(voteDTO);
				iterateId += 1;
			}

            voteDAO.addVote(voteDTOs, connection);
            connection.commit();
            connectionPool.releaseConnection(connection);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucessfully voted for the survey!", ""));
            return true;
        } catch (SQLException e) {
            if (connection != null) {
                try {
                    connection.rollback();
                    connectionPool.releaseConnection(connection);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_FATAL,
                            "Database error! Pls try it again after a while or contact the administrator if the error persists",
                            ""));
            e.printStackTrace();
            return false;
        }
    }
    public List<VoteDTO> getAllVotes(SurveyDTO survey) {
        Connection connection = null;
        List<VoteDTO> votes = null;
        try {
            connection = connectionPool.getConnection();
            votes = voteDAO.getVoteOfSurvey(survey.getSurveyId(), connection);
            connection.commit();
            connectionPool.releaseConnection(connection);
            return votes;
        } catch (SQLException e) {
            if (connection != null) {
                try {
                    connection.rollback();
                    connectionPool.releaseConnection(connection);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            e.printStackTrace();
            return new ArrayList<VoteDTO>();
        }
    }

	public List<QuestionResultDTO> getStatistic(SurveyDTO surveyDTO) {
		Connection connection = null;
		List<QuestionResultDTO> questionResultDTOs = new LinkedList<>();
        try {
            connection = connectionPool.getConnection();
            VoteDAO voteDAO = new VoteDAO();
            for (QuestionDTO questionID : surveyDTO.getQuestions()) {
            	QuestionResultDTO tempQuestionResultForTrueFalse = voteDAO.getQuestionResult(questionID.getQuestionId(), connection);
            	QuestionResultDTO tempQuestionResult = new QuestionResultDTO(questionID.getQuestionId(), questionID.getQuestionText() ,tempQuestionResultForTrueFalse.getResultYes(), tempQuestionResultForTrueFalse.getResultNo());
				questionResultDTOs.add(tempQuestionResult);
			}
            connection.commit();
            connectionPool.releaseConnection(connection);
            return questionResultDTOs;
        } catch (SQLException e) {
            if (connection != null) {
                try {
                    connection.rollback();
                    connectionPool.releaseConnection(connection);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            e.printStackTrace();
            return null;
        }
	}
}
