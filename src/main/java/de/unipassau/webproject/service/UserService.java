package de.unipassau.webproject.service;

import java.sql.Connection;
import java.sql.SQLException;

import de.unipassau.webproject.dataaccess.ConnectionPool;
import de.unipassau.webproject.dataaccess.UserDAO;
import de.unipassau.webproject.dto.UserDTO;

public class UserService {
    private UserDAO userDAO = new UserDAO();
    private ConnectionPool connectionPool = ConnectionPool.getInstance();

    public UserDTO getUserNameForId(long userId) {
        UserDTO databaseUser = null;
        Connection connection = null;

        try {
            connection = connectionPool.getConnection();
            databaseUser = userDAO.getUserById(userId, connection);
            connection.commit();
            connectionPool.releaseConnection(connection);
            return databaseUser;
        } catch (SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    connectionPool.releaseConnection(connection);
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
            return null;
        }
    }
}
