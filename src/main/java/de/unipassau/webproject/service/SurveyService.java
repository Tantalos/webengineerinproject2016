package de.unipassau.webproject.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import de.unipassau.webproject.dataaccess.ConnectionPool;
import de.unipassau.webproject.dataaccess.SurveyDAO;
import de.unipassau.webproject.dataaccess.VoteDAO;
import de.unipassau.webproject.dto.SurveyDTO;
import de.unipassau.webproject.dto.UserDTO;
import de.unipassau.webproject.dto.VoteDTO;

public class SurveyService {
    private ConnectionPool connectionPool = ConnectionPool.getInstance();
    private SurveyDAO surveyDAO = new SurveyDAO();

    public List<SurveyDTO> getOwnSurveyDTOs(UserDTO user) {
        Connection connection = null;
        List<SurveyDTO> ownSurveys = null;
        try {
            connection = connectionPool.getConnection();
            ownSurveys = surveyDAO.getSurveyByUserName(user, connection);
            connection.commit();
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    connectionPool.releaseConnection(connection);
                }

            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
                    "Database error!", "Try again later or contact the server admin if this error persist"));
            return null;
        }

        assert ownSurveys != null;
        return ownSurveys;
    }

    public List<SurveyDTO> getAllSurveyDTOs(UserDTO user) {
        List<SurveyDTO> allSurveys = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            allSurveys = surveyDAO.getALLSurveys(user, connection);
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    connectionPool.releaseConnection(connection);
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
                    "Database error!", "Try again later or contact the server admin if this error persist"));
            return null;
        }

        assert allSurveys != null;
        return allSurveys;
    }

    public List<SurveyDTO> getAllSurveyDTOsFromAllUsers() {
        List<SurveyDTO> allSurveys = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            allSurveys = surveyDAO.getALLSurveysFromAll(connection);
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    connectionPool.releaseConnection(connection);
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
                    "Database error!", "Try again later or contact the server admin if this error persist"));
            return null;
        }

        assert allSurveys != null;
        return allSurveys;
    }

    public String addSurvey(SurveyDTO survey) {
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            surveyDAO.addSurvey(survey, connection);
            connection.commit();
            connectionPool.releaseConnection(connection);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Survey successfully added!", ""));
            return "survey";
        } catch (SQLException e) {
            if (connection != null) {
                try {
                    connection.rollback();
                    connectionPool.releaseConnection(connection);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
                    "Database error!", "Try again later or contact the server admin if this error persist"));
            return "error";
        }

    }

    public String edit(SurveyDTO survey, UserDTO userDTO) {
        if (!isSurveyChangeable(survey)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Cannot edit survey because other users voted already!", ""));
            return "survey";
        }
        if (!isSurveyFromUser(survey, userDTO)) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cannot edit survey because it's not yours!", ""));
            return "survey";
        }
        return updateSurvey(survey);
    }

    public String delete(SurveyDTO survey, UserDTO userDTO) {
        if (!isSurveyChangeable(survey)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Cannot delete survey because other users voted already!", ""));
            return "survey";
        }
        if (!isSurveyFromUser(survey, userDTO)) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cannot delete survey because it's not yours!", ""));
            return "survey";
        }
        return deleteSurvey(survey);
    }

    private boolean isSurveyFromUser(SurveyDTO surveyDTO, UserDTO userDTO) {
        return surveyDTO.getCreator().equals(userDTO.getUserId());
    }

    private String deleteSurvey(SurveyDTO surveyDTO) {
        Connection connection = null;

        try {
            connection = connectionPool.getConnection();
            surveyDAO.deleteSurvey(surveyDTO, connection);
            connection.commit();
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            if (connection != null) {
                try {
                    connection.rollback();
                    connectionPool.releaseConnection(connection);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            e.printStackTrace();
            return "error";
        }
        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Survey successfully deleted", ""));
        return "survey.xhtml?faces-redirect=true";
    }

    public boolean isSurveyChangeable(SurveyDTO survey) {
        VoteService voteService = new VoteService();
        if(voteService.getVoteCount(survey) == 0){
        	return true;
        } else {
        	return false;
        }
    }

    private String updateSurvey(SurveyDTO surveyDTO) {
        Connection connection = null;

        try {
            connection = connectionPool.getConnection();
            surveyDAO.updateSurvey(surveyDTO, connection);
            connection.commit();
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            if (connection != null) {
                try {
                    connection.rollback();
                    connectionPool.releaseConnection(connection);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            e.printStackTrace();
            return "error";
        }
        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Survey successfully changed", ""));
        return "survey";
    }

    public boolean isSurveyVoteableFromUser(SurveyDTO surveyDTO, String username) {
		Connection connection = null;

        try {
            connection = connectionPool.getConnection();
            VoteDAO voteDAO = new VoteDAO();
            List <VoteDTO> votes = voteDAO.getVoteOfSurvey(surveyDTO.getSurveyId(), connection);
            for (VoteDTO voteDTO : votes) {
                if (username.equals(voteDTO.getVoter())) {
					return false;
				}
			}

            connection.commit();
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            if (connection != null) {
                try {
                    connection.rollback();
                    connectionPool.releaseConnection(connection);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_FATAL, "Database Error by loading rights to vote!", ""));
            return false;
        }
        return true;
	}

	public SurveyDTO getSurveyByID(long surveyID) {
		SurveyService surveyService = new SurveyService();
		List <SurveyDTO> surveyDTOs = surveyService.getAllSurveyDTOsFromAllUsers();
		for (SurveyDTO surveyDTO : surveyDTOs) {
			if(surveyDTO.getSurveyId().equals(surveyID)){
				return surveyDTO;
			}
		}
		return null;
	}
}
