package de.unipassau.webproject.service;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Random;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import de.unipassau.webproject.beans.SessionBean;
import de.unipassau.webproject.dataaccess.ConnectionPool;
import de.unipassau.webproject.dataaccess.UserDAO;
import de.unipassau.webproject.dto.UserDTO;
import de.unipassau.webproject.utils.EncryptionUtils;

public class LoginService {

    private ConnectionPool connectionPool = ConnectionPool.getInstance();
    private UserDAO userDAO = new UserDAO();


    public boolean checkPassword(UserDTO userDTO) {
        UserDTO databaseUser = null;
        Connection connection = null;

        try {
            connection = connectionPool.getConnection();
            databaseUser = userDAO.getUserByName(userDTO.getName(), connection);
            connection.commit();
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    connectionPool.releaseConnection(connection);
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
                    "Database error!", "Try again later or contact the server admin if this error persist"));
            return false;
        }

        if (databaseUser == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Incorrect Username and Password", "Please enter correct Username and Password"));
            return false;
        }

        assert databaseUser.getSalt() != "";
        assert databaseUser.getPasswordHash() != "";

        String hashedPassword = EncryptionUtils.hashString(userDTO.getPasswordHash() + databaseUser.getSalt());
        if (databaseUser.getPasswordHash().equals(hashedPassword)) {
            HttpSession session = SessionBean.getSession();
            session.setAttribute("user", databaseUser);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Login was successful", "Now work on your surveys"));
            return true;
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Incorrect Username and Password", "Please enter correct username and Password"));
            return false;
        }
    }

    /**
     * This is dummy function to fill database
     */
    @Deprecated
    public void registerUser() {
        String salt = new BigInteger(50, new Random()).toString(32);
        Connection connection = null;

        try {
            connection = connectionPool.getConnection();
            UserDTO user = new UserDTO("user", EncryptionUtils.hashString("1234" + salt), salt);
            userDAO.createUser(user, connection);
            connection.commit();
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    connectionPool.releaseConnection(connection);
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
    }

	public boolean isSigned() {
		HttpSession session = SessionBean.getSession();
		try{
			session.getAttribute("user");
			return true;
		} catch(Exception e) {
			return false;
		}
	}
}
