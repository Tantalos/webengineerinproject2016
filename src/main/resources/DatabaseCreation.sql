CREATE TABLE online_user (
	USER_ID BIGSERIAL PRIMARY KEY,
	USER_NAME VARCHAR(50) NOT NULL UNIQUE,
	BIRTH TIMESTAMP,
	ADDRESS VARCHAR(50),
	PASSWORDHASH VARCHAR(257),
	SALT VARCHAR(256)
);


CREATE TABLE survey (
	SURVEY_ID BIGSERIAL PRIMARY KEY,
	CREATOR bigint REFERENCES online_user(USER_ID),
	CREATION_DATE TIMESTAMP NOT NULL,
	TITLE VARCHAR(50) NOT NULL
);


CREATE TABLE question (
	QUESTION_ID BIGSERIAL PRIMARY KEY,
	SURVEY bigint REFERENCES survey(SURVEY_ID),
	QUESTION_TEXT VARCHAR(300)
);

CREATE TABLE vote (
	VOTE_ID BIGSERIAL PRIMARY KEY,
	VOTER VARCHAR(50) NOT NULL,
	QUESTION bigint REFERENCES question(QUESTION_ID),
	ANSWER BOOLEAN NOT NULL,
	CREATION_DATE timestamp NOT NULL
);

INSERT INTO online_user (user_name, birth, address, passwordhash, salt) VALUES ('user','2016-05-29 14:54:33.833','Schweden','5d18833b79403d882fcbf2c5ad7db04c2f4fb8e1dea5474c2a8bd5edfa7a9f6d','59io5c27fe');
INSERT INTO online_user (user_name, birth, address, passwordhash, salt) VALUES ('user2','2015-05-30 21:31:52.470000 +02:00:00','Russland','011e2a833253203715fb1b2fc24deb566b0215b905f7847cd482616c63e06831','f67sr3cn7o');
INSERT INTO online_user (user_name, birth, address, passwordhash, salt) VALUES ('user3','2014-05-30 21:31:52.470000 +02:00:00','Passau','472985c37872c970da3b78f97b1698ea4bc26c37cbbf05b72ff533b4204202f6','thisisasalt');
INSERT INTO online_user (user_name, birth, address, passwordhash, salt) VALUES ('user4','2013-05-30 21:31:52.470000 +02:00:00','Berlin','1227d7055230ad69b9df5959157f25eee27b5940213d1fc1d288f350a508270f','knasd62348nvbdf');
INSERT INTO online_user (user_name, birth, address, passwordhash, salt) VALUES ('user5','2012-05-30 21:31:52.470000 +02:00:00','Hamburg','03d78e7cf276358fe37b50ea2d922c783139b97246d144a6058e431295e2ea59','b9df5959157f25eee');
INSERT INTO online_user (user_name, birth, address, passwordhash, salt) VALUES ('user6','2011-05-30 21:31:52.470000 +02:00:00','Düsseldorf','72d1c711750fac7bcdc6539abd1047c7692f5f5c5f71c1dc84dbdc55cbc702d6','d922c783xx7cf2');

