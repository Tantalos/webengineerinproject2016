# JSF Poll Projekt Beschreibung

JSF-Poll ist eine Umfragesystem, welches im Rahmen einer Projektarbeit entstanden ist. Eserm�glicht Nutzern Umfragen (vom Typ Ja/Nein Fragen) zu erstellen, die von Jedermann beantwortet werden k�nnen. Das System unterst�tzt rudiment�re Verwaltungsfunktionen.

##Installation
Das Projekt l�sst sich mit maven builden. Maven l�st alle Abh�ngigkeiten auf und l�d die genutzten Bibliotheken automatisch herunter. Es muss lediglich eine Datenbank zur verf�gung gestellt werden. Zum builden muss maven im Projektverzeichnis mit folgendem Befehl ausgef�hrt werden.

```
mvn clean install
```

Die gebuildete .war Datei wird im Unterverzeichnis target/ abgelegt.

Alternativ kann das Projekt in eclipse als maven Projekt importiert werden. Hierbei ist darauf zu achten, dass in eclipse die richtige Laufzeitumgebung eingestllt ist (jdk 1.8)! 

###Datenbank
Das Projekt nutzt eine PostgreSQL Datenbank. Zugangsdaten und Konfiguration kann in /src/main/resources/database.properties angepasst werden. Die Standard Zugangsdaten sind:

database.user=testuser
database.password=hxOMDmUWWqpEwzj7OPBh

Mit dem Datenbankskript DatabaseCreation.sql in /src/main/resources k�nnen die n�tigen manuel Tabellen erzeugt und Standard Nutzer zur Datenbank hinzugef�gt werden. Die fertigen Nutzer haben das hei�en (user, user2, user3...) mit Passwort 1234.
ACHTUNG: Skript funktioniert nur f�r leere Datenbanken!

Zus�tslich k�nnen mit dem Datenbankskript DataSets.sql einigen dummy Daten zur Datenbank hizugef�gt werden.

###In eclipse bzw. IntelliJ
Das projekt nutzt die lombok library. F�r DTOs werden gettter, setter etc mit lombok generiert. Um in der IDE keinen Syntaxfehler zu haben muss f�r eclipse [lombok](https://projectlombok.org/) installiert werden. 

F�r IntelliJ existiert ein plugin, das installiert werden muss.

Das Projekt l�sst sich ohne weiteres mit maven builden. Die library wird nur f�r die IDE gebraucht.

###Webseite
Die Seite ist erreichbar unter <container home>/JSF-Poll/


####Besonderheiten
Die Modellierung von Umfragen wurde allgemein gehalten. Das Datenbankmodel w�rde es erlauben, dass Umfragen eine beliebige Anzahl an Fragen enthalten.

Datenbankabragen sind in Transitions organisiert. Mehrere Abfragen werden hierbei in Transitions zusammen gefasst. Ganze Transitions k�nnen bei einem Fehler vollst�ndig zur�ck gerollt werden.

Abklingender ConnectionPool: Zum ConnectionPool wurde zus�tlich ein Workerthread implementiert, der �bersch�ssige Verbindungen langsam abbaut. Verbindungen werden dabei nicht mehr bei einem connection release geschlossen.

Die Passw�rter der Nutzer werden mit enem SALT gehasht.
	 
